let posts = []; // Mock Database

let count = 1;

// Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        message: document.querySelector('#txt-message').value
    });

    // increment para yung count magkaron ng count ID (1,2,3....)
    count++;

    showPosts(posts)
    console.log(posts)
    alert('Successfully added.')

});

const showPosts = (posts) => {

    let postEntries = '';

    // loop inside array
    posts.forEach((post) => {

   
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}"> ${post.title}</h3>
                <p id="post-message-${post.id}"> ${post.message}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
        console.log()
    });
    document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// EDIT POST
const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let message = document.querySelector(`#post-message-${id}`).innerHTML;
    
    // reassigning
    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-message').value = message;
    
};


// UPDATE POST
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

    e.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        
        if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {

            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].message = document.querySelector('#txt-edit-message').value;

            showPosts(posts);
            alert("Successfully Updated!")

            break;
        };
    };
});


// DELETE POST

// Solution 1
function deletePost(){

    posts.pop();
    showPosts(posts);
};


// Solution 2
const deletePost = (id) => {

    posts = posts.filter((post) => {
        if(post.id.toString() !== id){
            console.log(post)
            return post
        };
    });
    document.querySelector(`#post-${id}`).remove();
    console.log(posts);
};


// Solution 3
let indexToDelete = posts.findIndex(posts => posts.id == id);
    posts.splice(indexToDelete, 1)
    document.querySelector(`#post-${id}`).remove();

showPosts(posts);
alert('Successfully Deleted!');

